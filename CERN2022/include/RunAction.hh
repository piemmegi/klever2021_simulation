//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file RunAction.hh
/// \brief Definition of the B1::RunAction class

#ifndef B1RunAction_h
#define B1RunAction_h 1

#include "G4UserRunAction.hh"
#include "G4Accumulable.hh"
#include "G4AutoLock.hh"
#include <vector>
#include "globals.hh"
#include <fstream>

class G4Run;

/// Run action class
///
/// In EndOfRunAction(), it calculates the dose in the selected volume
/// from the energy deposit accumulated via stepping and event actions.
/// The computed dose is then printed on the screen.

class RunAction : public G4UserRunAction
{
  public:
    RunAction();
    ~RunAction() override;

    void BeginOfRunAction(const G4Run*) override;
    void EndOfRunAction(const G4Run*) override;

	
	// Fill the energies of the detectors
	void AddDepositedEnergyGenni(G4double Edep) {E_Genni.push_back(Edep);}
	void AddDepositedEnergyJack(G4double Edep) {E_Jack.push_back(Edep);}

	void AddDepositedEnergyLG1(G4double Edep) {E_LG1.push_back(Edep);}
	void AddDepositedEnergyLG2(G4double Edep) {E_LG2.push_back(Edep);}
	void AddDepositedEnergyLG3(G4double Edep) {E_LG3.push_back(Edep);}
	void AddDepositedEnergyLG4(G4double Edep) {E_LG4.push_back(Edep);}
	void AddDepositedEnergyLG5(G4double Edep) {E_LG5.push_back(Edep);}
	void AddDepositedEnergyLG6(G4double Edep) {E_LG6.push_back(Edep);}
	void AddDepositedEnergyLG7(G4double Edep) {E_LG7.push_back(Edep);}

	void AddDepositedEnergyCrystal(G4double Edep) {E_Crys.push_back(Edep);}
	void AddDepositedEnergyCanadese(G4double Edep) {E_Canadese.push_back(Edep);}

  private:

	// Define the variables for the calorimeters
	std::vector<G4double> E_Genni;
	std::vector<G4double> E_Jack;
	std::vector<G4double> E_LG1;
	std::vector<G4double> E_LG2;
	std::vector<G4double> E_LG3;
	std::vector<G4double> E_LG4;
	std::vector<G4double> E_LG5;
	std::vector<G4double> E_LG6;
	std::vector<G4double> E_LG7;
	std::vector<G4double> E_Crys;
	std::vector<G4double> E_Canadese;
};

#endif

