//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file DetectorConstruction.cc
/// \brief Implementation of the B1::DetectorConstruction class

#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4SystemOfUnits.hh"
#include "G4RegionStore.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"


// Color
#include "G4VisAttributes.hh"
#include "G4Colour.hh"

DetectorConstruction::DetectorConstruction()
{}

DetectorConstruction::~DetectorConstruction()
{}

G4VPhysicalVolume* DetectorConstruction::Construct()
{
    //===================================================================================================
    //                                       DEFINITION OF THE MATERIALS
    //===================================================================================================
    // Get NIST material manager
    // (see https://geant4-userdoc.web.cern.ch/UsersGuides/ForApplicationDeveloper/html/Appendix/materialNames.html)
    G4NistManager* nist = G4NistManager::Instance();

    G4Material* Silicon = nist->FindOrBuildMaterial("G4_Si");
    G4Material* Copper= nist->FindOrBuildMaterial("G4_Cu");
    G4Material* Lead = nist->FindOrBuildMaterial("G4_Pb");
    G4Material* Plastic= nist->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE");
    G4Material* BGO = nist->FindOrBuildMaterial("G4_BGO");
    G4Material* PbWO4 = nist->FindOrBuildMaterial("G4_PbWO4");

    // Define variables for building complex materials
    G4double density;
    G4int ncomponents;
    G4int natoms;

    // Use the G4 database on G4Elements to build the materials used in the setup:
    // first of all, build lead glass as PbO
    //  G4Material* PbO = new G4Material("PbO", density=  9.530 *g/cm3, ncomponents=2);
    //  G4Element* O  = nist->FindOrBuildElement(8);
    //  PbO->AddElement(O , natoms=1);
    //  G4Element* Pb = nist->FindOrBuildElement(82);
    //  PbO->AddElement(Pb, natoms= 1);

    // Use the G4 model of lead glass
    G4Material* PbO = nist->FindOrBuildMaterial("G4_GLASS_LEAD");
    
    // Build the air (used for the overworld and the magnet)
    G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");

    // Build manually the PbF2, since it does not exist in the NIST repository
    G4Element* Pb = nist->FindOrBuildElement(82);
    G4Element* F = nist->FindOrBuildElement(9);
    density = 7.770 *g/cm3;

    G4Material* PbF2 = new G4Material("PbF2", density, ncomponents= 2);
	
    PbF2->AddElement(Pb, natoms=1);
    PbF2->AddElement(F, natoms=2);

    //===================================================================================================
    //									DEFINITION OF THE WORLD
    //===================================================================================================
    // Define the boolean variable for the checks in the overlaps
    G4bool checkOverlaps = true;

    // Build the WORLD volume. For this volume and for the others, all volumes (solid, logical, physical)
    // should be built separately
    G4Box* solidWorld = new G4Box("World",10*m,10*m,50*m);
    G4LogicalVolume* logicWorld = new G4LogicalVolume(solidWorld, world_mat, "World");
    G4VPhysicalVolume* physWorld = new G4PVPlacement(0,                    // no rotation
                        G4ThreeVector(),       // centre position
                        logicWorld,            // its logical volume
                        "World",               // its name
                        0,                     // its mother volume
                        false,                 // no boolean operation
                        0,                     // copy number
                        checkOverlaps);        // overlaps checking

    // Hide the overworld from a visual point of view (otherwise you can't see inside it in the rendering)
    G4VisAttributes* worldColor= new G4VisAttributes(G4Colour(1.,1.,1.));
    worldColor->SetVisibility(false);
    logicWorld->SetVisAttributes(worldColor);
	
    //===================================================================================================
    // 									DEFINITION OF THE DETECTORS (no calo)
    //===================================================================================================
    // Telescope 1
    G4Box* Tele1 = new G4Box("Tele1",2*cm/2, 2*cm/2, 410*um/2);
    G4LogicalVolume* fLogicTele1 = new G4LogicalVolume(Tele1, Silicon,"Tele1");
    new G4PVPlacement(0, 
                      G4ThreeVector(0, 0, 0.*m), 
                      fLogicTele1, 
                      "Tele1", 
                      logicWorld, 
                      false, 
                      0, 
                      checkOverlaps);

    // Telescope 2
    G4Box* Tele2 = new G4Box("Tele2",2*cm/2, 2*cm/2, 410*um/2);
    G4LogicalVolume* fLogicTele2 = new G4LogicalVolume(Tele2, Silicon,"Tele2");
    new G4PVPlacement(0, 
                      G4ThreeVector(0, 0, 13.65*m), 
                      fLogicTele2, 
                      "Tele2", 
                      logicWorld, 
                      false, 
                      0, 
                      checkOverlaps);

    // MCP: missing, since I don't know which material to use for its creation

    // Bremsstrahlung target
    G4Box* Rame = new G4Box("BStarget",10*cm/2, 10*cm/2, 3*mm/2);
    G4LogicalVolume* fLogicRame = new G4LogicalVolume(Rame, Copper,"BStarget");
    new G4PVPlacement(0, 
                      G4ThreeVector(0., 0, 14.43*m), 
                      fLogicRame, 
                      "BStarget", 
                      logicWorld, 
                      false, 
                      0, 
                      checkOverlaps);

    // Beam Chamber 1
    G4Box* BC1 = new G4Box("BC1",10*cm/2, 10*cm/2, 410*um/2);
    G4LogicalVolume* fLogicBC1 = new G4LogicalVolume(BC1, Silicon,"BC1");
    new G4PVPlacement(0, 
                      G4ThreeVector(0, 0, 18.90*m), 
                      fLogicBC1, 
                      "BC1", 
                      logicWorld, 
                      false, 
                      0, 
                      checkOverlaps);

    // Magnetic field volume (air box to whom the field will be attached)
    G4Box* Magnet = new G4Box("Magnet",20*cm/2, 20*cm/2, 2*m/2); // in x,y: approximated!
    G4LogicalVolume* logicMagnet = new G4LogicalVolume(Magnet,world_mat,"Magnet");
    new G4PVPlacement(0,
                      G4ThreeVector(0, 0, 18.90*m + 65*cm + 2*m/2),
                      logicMagnet,
                      "Magnet",
                      logicWorld,
                      false,
                      0,
                      checkOverlaps);

    // Target crystal (+ SiPMs, which are here not modelled). Pay attention: the volume is expressed
    // in units of X0
    G4bool ifPbF2 = true;           // "true" -> PbF2; "false" -> PbWO4
    G4double thicknessCrist;        // I have to declare the variables in the global scope, then fill them
    G4Box* Crystal;
    G4LogicalVolume* logicCrystal;

    if (ifPbF2) {
        // Assume the crystal is PbF2
        thicknessCrist = 2 * PbF2->GetRadlen();
        Crystal = new G4Box("Crystal", 1*PbF2->GetRadlen()/2, 2*PbF2->GetRadlen()/2, thicknessCrist/2);
        logicCrystal = new G4LogicalVolume(Crystal,PbF2,"Crystal");
    }
    else {
        // Assume the crystal is PbWO4
        thicknessCrist = 1 * PbWO4->GetRadlen();
        Crystal = new G4Box("Crystal", 30*mm/2, 30*mm/2, thicknessCrist/2);
        logicCrystal = new G4LogicalVolume(Crystal,PbWO4,"Crystal");
    }
    
    new G4PVPlacement(0,
                      G4ThreeVector(0, 0, 28.67*m),
                      logicCrystal,
                      "Crystal",
                      logicWorld,
                      false,
                      0,
                      checkOverlaps);

    // Scinti Canadese (Multiplicity Counter)
    G4Box* ScintiCanada = new G4Box("ScintiCanada",10*cm/2, 10*cm/2, 5*cm/2);
    G4LogicalVolume* fLogicScintiCanada = new G4LogicalVolume(ScintiCanada, Plastic,"ScintiCanada");
    new G4PVPlacement(0, 
                      G4ThreeVector(0*cm, 0, 32.46*m), 
                      fLogicScintiCanada, 
                      "ScintiCanada", 
                      logicWorld, 
                      false, 
                      0, 
                      checkOverlaps);

    // Beam Chamber 2
    G4Box* BC2 = new G4Box("BC2",10*cm/2, 10*cm/2, 410*um/2);
    G4LogicalVolume* fLogicBC2 = new G4LogicalVolume(BC2, Silicon,"BC2");
    new G4PVPlacement(0, 
                      G4ThreeVector(0, 0, 32.61*m), 
                      fLogicBC2, 
                      "BC2", 
                      logicWorld, 
                      false, 
                      0, 
                      checkOverlaps);

    //===================================================================================================
    // 									DEFINITION OF THE CALORIMETERS
    //===================================================================================================
    // Define where the calorimeters are located
    G4double distCalo = 32.98*m;

    // GENNI (assumed to be perfectly on-axis)
    G4double z0_genni = distCalo + 23*cm/2;   // This is the center of Genni
    G4Box* Genni = new G4Box("Genni",15*cm/2, 15*cm/2, 23*cm/2);
    G4LogicalVolume* fLogicGenni = new G4LogicalVolume(Genni, BGO,"Genni");
    new G4PVPlacement(0, 
                      G4ThreeVector(0, 0, z0_genni), 
                      fLogicGenni, 
                      "Genni", 
                      logicWorld, 
                      false, 
                      0, 
                      checkOverlaps); 

    // JACK. Option 1: model as Lead Glass calorimeter, for simplicity
    //G4Box* Jack = new G4Box("Jack", 10*cm/2, 10*cm/2, 39*cm/2);
    //G4LogicalVolume* fLogicJack = new G4LogicalVolume(Jack, Lead,"Jack");
    //new G4PVPlacement(0, 
    //				G4ThreeVector(17.3*cm, .25*cm, z0_genni + 31*cm), 
    //				fLogicJack, 
    //				"Jack", 
    //				logicWorld, 
    //				false, 
    //				0, 
    //				checkOverlaps); 

    // JACK. Option 2: model as Shashlik Sampling Calorimeter
    G4double offsetJack = 31*cm;  // From center of Genni to center of Jack
    
    G4Box* JackPlastic = new G4Box("JackPlastic", 11.5*cm/2, 11.5*cm/2, 4*mm/2);
    G4LogicalVolume* fLogicJackPlastic = new G4LogicalVolume(JackPlastic, Plastic,"JackPlastic");
    
    G4Box* JackLead = new G4Box("JackLead", 11.5*cm/2, 11.5*cm/2, 1.5*mm/2);
    G4LogicalVolume* fLogicJackLead = new G4LogicalVolume(JackLead, Lead,"JackLead");
    
    // Iteratively place each layer of lead and scintillator
    for (int i=0 ; i<70 ; i++) {
        // Set the i-th layer of plastic scintillator
        new G4PVPlacement(0, 
                      G4ThreeVector(17.3*cm, 0*cm, z0_genni + offsetJack + i * 5.5*mm), 
                      fLogicJackPlastic, 
                      "JackPlastic",
                      logicWorld,
                      false,
                      i, // Not sure it is vital, probably not
                      checkOverlaps);
        
        // Set the i-th layer of lead
        new G4PVPlacement(0, 
                      G4ThreeVector(17.3*cm, 0*cm, z0_genni + offsetJack + (5.5/2)*mm + i * 5.5*mm), // Sommo metà del blocco
                      fLogicJackLead, 
                      "JackLead",
                      logicWorld,
                      false,
                      i, // Not sure it is vital, probably not
                      checkOverlaps);
    }
    
    // Set the last scintillating layer
    new G4PVPlacement(0, 
                  G4ThreeVector(17.3*cm, 0*cm, z0_genni + offsetJack + 70 * 5.5*mm), // TODO: Bisogna sommare qualcosa per allinearlo in z
                  fLogicJackPlastic, 
                  "JackPlastic",
                  logicWorld,
                  false,
                  75, // Not sure it is vital, probably not
                  checkOverlaps);

    // Here begins the Lead Glasses section. Note: define preliminarly their rotation matrixes.
    G4double angle1 = 1.5*degree ; // LG1 (the second)
    G4RotationMatrix *xRot1 = new G4RotationMatrix;
    xRot1->rotateY(-angle1);

    G4double angle2 = 4.5*degree ; // LG3-LG7 (from the third onwards)
    G4RotationMatrix *xRot2 = new G4RotationMatrix;
    xRot2->rotateY(-angle2);

    // LG 2
    G4Box* LG2 = new G4Box("LG2", 11.5*cm/2, 11.5*cm/2, 31.2*cm/2);
    G4LogicalVolume* fLogicLG2 = new G4LogicalVolume(LG2, PbO,"LG2");
    new G4PVPlacement(0, 
                      G4ThreeVector(32.9*cm, 0*cm, z0_genni + 32.2*cm), 
                      fLogicLG2, 
                      "LG2", 
                      logicWorld, 
                      false, 
                      0, 
                      checkOverlaps); 

    // LG 1			
    G4Box* LG1 = new G4Box("LG1", 11.5*cm/2, 11.5*cm/2, 31.2*cm/2);
    G4LogicalVolume* fLogicLG1 = new G4LogicalVolume(LG1, PbO,"LG1");
    new G4PVPlacement(xRot1, 
                      G4ThreeVector(43.5*cm, 0*cm, z0_genni + 31.7*cm), 
                      fLogicLG1, 
                      "LG1", 
                      logicWorld, 
                      false, 
                      0, 
                      checkOverlaps); 

    // LG 7
    G4double x0_LG = 56.6*cm;
    G4double z0_LG = z0_genni + 30.5*cm;
	
    G4Box* LG7 = new G4Box("LG7", 11.5*cm/2, 11.5*cm/2, 31.2*cm/2);
    G4LogicalVolume* fLogicLG7 = new G4LogicalVolume(LG7, PbO,"LG7");
    new G4PVPlacement(xRot2, 
                      G4ThreeVector(x0_LG, 0*cm, z0_LG), 
                      fLogicLG7, 
                      "LG7", 
                      logicWorld, 
                      false, 
                      0, 
                      checkOverlaps); 

    // LG 5
    G4Box* LG5 = new G4Box("LG5", 11.5*cm/2, 11.5*cm/2, 31.2*cm/2);
    G4LogicalVolume* fLogicLG5 = new G4LogicalVolume(LG5, PbO,"LG5");
    new G4PVPlacement(xRot2, 
                      G4ThreeVector(x0_LG + 11.5*cm*cos(angle2), 0*cm, z0_LG - 11.5*cm*sin(angle2)), 
                      fLogicLG5, 
                      "LG5", 
                      logicWorld, 
                      false, 
                      0, 
                      checkOverlaps); 

    // LG 6
    G4Box* LG6 = new G4Box("LG6", 11.5*cm/2, 11.5*cm/2, 31.2*cm/2);
    G4LogicalVolume* fLogicLG6 = new G4LogicalVolume(LG6, PbO,"LG6");
    new G4PVPlacement(xRot2, 
                      G4ThreeVector(x0_LG + 2*11.5*cm*cos(angle2), -1.5*cm, z0_LG - 2*11.5*cm*sin(angle2)), 
                      fLogicLG6, 
                      "LG6", 
                      logicWorld, 
                      false, 
                      0, 
                      checkOverlaps); 

    // LG 3
    G4Box* LG3 = new G4Box("LG3", 11.5*cm/2, 11.5*cm/2, 31.2*cm/2);
    G4LogicalVolume* fLogicLG3 = new G4LogicalVolume(LG3, PbO,"LG3");
    new G4PVPlacement(xRot2, 
                      G4ThreeVector(x0_LG + 3*11.5*cm*cos(angle2), -1.5*cm, z0_LG - 3*11.5*cm*sin(angle2)), 
                      fLogicLG3, 
                      "LG3", 
                      logicWorld, 
                      false, 
                      0, 
                      checkOverlaps); 

    // LG 4 
    G4Box* LG4 = new G4Box("LG4", 11.5*cm/2, 11.5*cm/2, 31.2*cm/2);
    G4LogicalVolume* fLogicLG4 = new G4LogicalVolume(LG4, PbO,"LG4");
    new G4PVPlacement(xRot2, 
                      G4ThreeVector(x0_LG + 4*11.5*cm*cos(angle2), -1.5*cm, z0_LG - 4*11.5*cm*sin(angle2)), 
                      fLogicLG4, 
                      "LG4", 
                      logicWorld, 
                      false, 
                      0, 
                      checkOverlaps); 

    //===================================================================================================
    // 									PARAMETERS FOR THE GRAPHICS
    //===================================================================================================
    // Silicon trackers: = Verdi
    G4VisAttributes* siliColor = new G4VisAttributes(G4Colour(0.,1.,0., 1.));
    siliColor->SetVisibility(true);
    siliColor->SetForceSolid(true);
    
    fLogicTele1->SetVisAttributes(siliColor);
    fLogicTele2->SetVisAttributes(siliColor);
    fLogicBC1->SetVisAttributes(siliColor);
    fLogicBC2->SetVisAttributes(siliColor);
    
    // Copper target, target crystal, Canadese: = red
    G4VisAttributes* absColor = new G4VisAttributes(G4Colour(1.,0.,0., 1.));
    absColor->SetVisibility(true);
    absColor->SetForceSolid(true);
    
    fLogicRame->SetVisAttributes(absColor);
    logicCrystal->SetVisAttributes(absColor);
    fLogicScintiCanada->SetVisAttributes(absColor);
    
    // Magnet : = yellow
    G4VisAttributes* magColor = new G4VisAttributes(G4Colour(1.,1.,0., .5));
    magColor->SetVisibility(true);
    magColor->SetForceSolid(true);
    
    logicMagnet->SetVisAttributes(magColor);
    
    // Calorimeters (even numbers) : = green
    G4VisAttributes* caloA = new G4VisAttributes(G4Colour(0.,1.,0., 1.));
    caloA->SetVisibility(true);
    caloA->SetForceSolid(true);
    
    fLogicGenni->SetVisAttributes(caloA);
    fLogicLG2->SetVisAttributes(caloA);
    fLogicLG7->SetVisAttributes(caloA);
    fLogicLG6->SetVisAttributes(caloA);
    fLogicLG4->SetVisAttributes(caloA);
    
    // Calorimeters (odd numbers) : = blue
    G4VisAttributes* caloB = new G4VisAttributes(G4Colour(0.,0.,1., 1.));
    caloB->SetVisibility(true);
    caloB->SetForceSolid(true);
    
    //fLogicJack->SetVisAttributes(caloB);
    fLogicLG1->SetVisAttributes(caloB);
    fLogicLG5->SetVisAttributes(caloB);
    fLogicLG3->SetVisAttributes(caloB);
    
    // Jack: alternate yellow (plastic) and lead (red)
    fLogicJackPlastic->SetVisAttributes(magColor);
    fLogicJackLead->SetVisAttributes(absColor);
    
    // Always return the physical World!
    return physWorld;
}

void DetectorConstruction::ConstructSDandField()
{
    //===================================================================================================
    // 									MAGNETIC FIELD CONSTRUCTION
    //===================================================================================================
    // Define the magnetic field intensity and vector
    G4double fieldValue = 4.05778*tesla*m / (2*m);
    G4UniformMagField* myField = new G4UniformMagField(G4ThreeVector(0., fieldValue, 0.));

    // Obtain the global field manager
    G4FieldManager* fieldMgr = G4TransportationManager::GetTransportationManager()->GetFieldManager();

    // Define a local field manager (associated to the field vector chosen) and apply it to the magnet
    // logic volume
    G4LogicalVolume* logicBox1 = G4LogicalVolumeStore::GetInstance()->GetVolume("Magnet");
    G4FieldManager* localfieldMgr = new G4FieldManager(myField);
    logicBox1->SetFieldManager(localfieldMgr,true);

    // Apply the Chord Finder to the global (and thus local) manager
    fieldMgr->CreateChordFinder(myField);
}