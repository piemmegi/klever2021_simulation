//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file RunAction.cc
/// \brief Implementation of the B1::RunAction class

#include "RunAction.hh"
#include "PrimaryGeneratorAction.hh"
#include "DetectorConstruction.hh"
#include "G4AnalysisManager.hh"

#include "G4RunManager.hh"
#include "G4Run.hh"
#include "G4AccumulableManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

//namespace B1
namespace { G4Mutex stuffMutex = G4MUTEX_INITIALIZER; }

RunAction::RunAction()
    : G4UserRunAction()
{
    //using analysis manager for output
    auto analysisManager = G4AnalysisManager::Instance();
}

RunAction::~RunAction()
{}

void RunAction::BeginOfRunAction(const G4Run* run)
{
    G4RunManager::GetRunManager()->SetPrintProgress(run->GetNumberOfEventToBeProcessed()/100);

    // inform the runManager to save random number seed
    G4RunManager::GetRunManager()->SetRandomNumberStore(false);

    if (IsMaster()) {
      G4cout
       << G4endl
       << "--------------------Begin of Global Run-----------------------";
    }
    else {
        G4int ithread = G4Threading::G4GetThreadId();

        G4cout
         << G4endl
         << "--------------------Begin of Local Run------------------------"
         << G4endl;
    }

}

void RunAction::EndOfRunAction(const G4Run* run)
{

    G4int nofEvents = run->GetNumberOfEvent();

    // Run conditions
    //  note: There is no primary generator action object for "master"
    //        run manager for multi-threaded mode.

    // Print
    //
    if (IsMaster()) {
      G4cout
       << G4endl
       << "--------------------End of Global Run-----------------------";
    }
    else {

      G4cout
       << G4endl
       << "--------------------End of Local Run------------------------";



      static bool first = true;
      stuffMutex.lock();

      std::ofstream resPMG;

      if (first)
      {
        // Create the data file, if it does not exist, and fill it with the names of the columns
		resPMG.open("resPMG.dat");
        first = false;
		  
		resPMG<<"Genni\tJack\tLG1\tLG2\tLG3\tLG4\tLG5\tLG6\tLG7\tCrystal\tCanadese"<<std::endl;

      }
      else
      {
        // Just open the file
		resPMG.open("resPMG.dat", std::ios_base::app);
      }
	  
	  
	  // Fill all the columns with the corresponding values
      for (int i = 0; i < E_Genni.size(); i++)
      {
          resPMG<<E_Genni[i]<<"\t"<<E_Jack[i]<<"\t"<<E_LG1[i]<<"\t"<<E_LG2[i]<<"\t"<<E_LG3[i]<<"\t"<<E_LG4[i]<<"\t"<<E_LG5[i]<<"\t"<<E_LG6[i]<<"\t"<<E_LG7[i]<<"\t"<<E_Crys[i]<<"\t"<<E_Canadese[i]<<"\t"<<std::endl;

      }
	  
	  resPMG.close();
      stuffMutex.unlock();
    }

    // At the rend of the run, print the number of events
    G4cout
       << G4endl
       << " The run consists of " << nofEvents << " particles"
       << G4endl
       << "------------------------------------------------------------"
       << G4endl
       << G4endl;
}