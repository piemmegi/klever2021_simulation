//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file EventAction.cc
/// \brief Implementation of the B1::EventAction class

#include "EventAction.hh"
#include "RunAction.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"

EventAction::EventAction(RunAction* runAction)
: fRunAction(runAction)
{}

EventAction::~EventAction()
{}

void EventAction::BeginOfEventAction(const G4Event* event)
{
	// Set to zero the variables which will be filled in the event
	E_Genni = 0;
	E_Jack = 0;

	E_LG1 = 0;
	E_LG2 = 0;
	E_LG3 = 0;
	E_LG4 = 0;
	E_LG5 = 0;
	E_LG6 = 0;
	E_LG7 = 0;

	E_Crys = 0;
	E_Canadese = 0;
}

void EventAction::EndOfEventAction(const G4Event*)
{
    // At the end of each event, add to the energies the measured values
    fRunAction->AddDepositedEnergyGenni(E_Genni);
    fRunAction->AddDepositedEnergyJack(E_Jack);

    fRunAction->AddDepositedEnergyLG1(E_LG1);
    fRunAction->AddDepositedEnergyLG2(E_LG2);
    fRunAction->AddDepositedEnergyLG3(E_LG3);
    fRunAction->AddDepositedEnergyLG4(E_LG4);
    fRunAction->AddDepositedEnergyLG5(E_LG5);
    fRunAction->AddDepositedEnergyLG6(E_LG6);
    fRunAction->AddDepositedEnergyLG7(E_LG7);

    fRunAction->AddDepositedEnergyCrystal(E_Crys);
    fRunAction->AddDepositedEnergyCanadese(E_Canadese);
}