//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file SteppingAction.cc
/// \brief Implementation of the B1::SteppingAction class

#include "SteppingAction.hh"
#include "EventAction.hh"
#include "DetectorConstruction.hh"

#include "G4Step.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4LogicalVolume.hh"
#include "G4SystemOfUnits.hh"

#include "G4AnalysisManager.hh"

SteppingAction::SteppingAction(EventAction* eventAction)
: fEventAction(eventAction)
{}

SteppingAction::~SteppingAction()
{}

void SteppingAction::UserSteppingAction(const G4Step* step)
{
    // Get the volume where the particle is currently located
    G4LogicalVolume* volume
        = step->GetPreStepPoint()->GetTouchableHandle()
          ->GetVolume()->GetLogicalVolume();
		  
    // Depending on the volume where the particle is located, fill the corresponding variable
	if(volume->GetName()=="JackPlastic")
    {
        fEventAction->AddDepositedEnergyJack(step->GetTotalEnergyDeposit()/GeV);
    }

    if(volume->GetName()=="JackLead")
    {
        fEventAction->AddDepositedEnergyJack(step->GetTotalEnergyDeposit()/GeV);
    } 

    if(volume->GetName()=="Genni")
    {
        fEventAction->AddDepositedEnergyGenni(step->GetTotalEnergyDeposit()/GeV);
    }

	if(volume->GetName()=="LG1")
    {
        fEventAction->AddDepositedEnergyLG1(step->GetTotalEnergyDeposit()/GeV);
    }

	if(volume->GetName()=="LG2")
    {
        fEventAction->AddDepositedEnergyLG2(step->GetTotalEnergyDeposit()/GeV);
    }
	
    if(volume->GetName()=="LG3")
    {
        fEventAction->AddDepositedEnergyLG3(step->GetTotalEnergyDeposit()/GeV);
    }
	
    if(volume->GetName()=="LG4")
    {
        fEventAction->AddDepositedEnergyLG4(step->GetTotalEnergyDeposit()/GeV);
    }
	
    if(volume->GetName()=="LG5")
    {
        fEventAction->AddDepositedEnergyLG5(step->GetTotalEnergyDeposit()/GeV);
    }
	
    if(volume->GetName()=="LG6")
    {
        fEventAction->AddDepositedEnergyLG6(step->GetTotalEnergyDeposit()/GeV);
    }
	
    if(volume->GetName()=="LG7")
    {
        fEventAction->AddDepositedEnergyLG7(step->GetTotalEnergyDeposit()/GeV);
    }

    if(volume->GetName()=="Crystal")
    {
        fEventAction->AddDepositedEnergyCrystal(step->GetTotalEnergyDeposit()/GeV);
    }

    if(volume->GetName()=="ScintiCanada")
    {
        fEventAction->AddDepositedEnergyCanadese(step->GetTotalEnergyDeposit()/GeV);
    }
}