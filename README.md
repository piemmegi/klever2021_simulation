# KLEVER 2021 simulation framework

Complete simulation of the experimental setup used at the CERN SPS during August 2021 for the KLEVER Beam Test.

Author of the first (and biggest) draft: [S. Carsi](mailto:scarsi@studenti.uninsubria.it).

Author of the subsequent edits and maintenance fixes: [P. Monti-Guarnieri](mailto:pmontiguarnieri@studenti.uninsubria.it)

# A short guide on how to install Geant4 on WSL2

## (Install and) verify you have WSL2 (and not 1)
[This](https://winaero.com/update-from-wsl-to-wsl-2-in-windows-10/#:~:text=To%20update%20from%20WSL%20to%20WSL%202%20in%20Windows%2010%2C&text=Restart%20Windows%2010.,set%2Ddefault%2Dversion%202%20.) is the reference guide. Shortly

1. Enable *Windows Subsystem for Linux* and *Virtual Machine platform* via Power Shell (run as admin). It can be done also via control panel, under Programs and features -> the last item. If you already have a WSL, you can skip this step
```powershell
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
```
If you do it, you need to restart the system

2. Download and install the latest Linux kernel update package (Step 5 of the guide linked above)

3. Set WSL 2 as your default version (It's optional. But this way any new distro installed is configured as WSL 2)
```powershell
wsl --set-default-version 2
```
Now every distro you will install is as WSL2.

4. Install a distro for WSL. It is **highly recommended** to install Ubuntu 20.04.5 LTS; the important thing is **not to install any distro beyond Ubuntu 21.04 (included). The reason for this is the fact that since Ubuntu 21.04 onward, several packages needed for the installation of Geant4 have been dismissed. There are [workarounds for this problem](https://askubuntu.com/questions/1335184/qt5-default-not-in-ubuntu-21-04)., but the easiest solution is probably to just install Ubuntu 20.04.05 LTS.

4. Change your existing WSL to WSL2. First get the name of your distro and, if the version is 1, set it to 2 (First command check version, second eventually to set it)
```powershell
wsl -l -v
wsl --set-version Ubuntu 2
```
where `Ubuntu` is the name of my distro.

### Allowing WSL2 to output graphics
1. First of all, VcXsrv in necessary [Download here](https://sourceforge.net/projects/vcxsrv/)
2. Follow [this link](https://stackoverflow.com/questions/61110603/how-to-set-up-working-x11-forwarding-on-wsl2). Basically you have to add in `~/.bashrc`
```
export DISPLAY=$(awk '/nameserver / {print $2; exit}' /etc/resolv.conf 2>/dev/null):0
export LIBGL_ALWAYS_INDIRECT=1
```
And when startinx VcXsrv (you will find in start menu under `XLaunch`, you have to: Multiple windows > Start no client > Uncheck "Native opengl" and check "Disable access control"

### Backup commands

These commands are not really needed, so just skip them for now...

```powershell
systeminfo | find "System Type"
wsl.exe --list --all
wsl --set-default-version 2
wsl --set-version Ubuntu 2
wsl -l -v
wsl --update
wsl --shutdown
```

## Install CMake, Make and C/C++ compilers
Now let's install CMake, Make and also a C and a C++ compiler (i.e., gcc and g++). **Pay attention to the outputs of each command**, since Linux *will* tell you if there is any problem to be solved (and usually it will also tell you *how to solve it*). 

```bash
sudo apt purge cmake
sudo apt install libssl-dev

mkdir CMake && cd CMake
wget https://github.com/Kitware/CMake/releases/download/v3.18.2/cmake-3.18.2.tar.gz
sudo tar xzf cmake-3.18.2.tar.gz && cd cmake-3.18.2

sudo apt-get update
sudo apt install gcc
sudo apt install g++
sudo apt-get install build-essential

sudo ./bootstrap
sudo make -j4
sudo make install
```
The boostrap and make phase should be the longer, while the install phase should be very quick.

## Install some more dependencies

```bash
sudo apt -y install libglu1-mesa-dev freeglut3-dev mesa-common-dev xorg
sudo apt -y install qtcreator qt5-default
sudo add-apt-repository ppa:rock-core/qt4
sudo apt update
sudo apt install libqt4-declarative qt4-dev-tools qt4-qmake libqtwebkit4
sudo apt -y install libxmu-dev libxerces-c-dev libexpat1-dev libssl-dev
sudo apt -y update && sudo apt -y upgrade
```

## Installation of Geant4 11.0.2
This version is the one within which the simulation was developed and tested, so it is the one recommended. It is generally compatible with Alex Sytov's simulations.

1. Create a folder and enter it
```bash
mkdir Geant4.11.0.2 && cd Geant4.11.0.2
```

2. Download the desired version (format .tar.gz) and extract it. You can find a complete list [here](https://github.com/Geant4/geant4/releases)
```bash
wget https://github.com/Geant4/geant4/archive/refs/tags/v11.0.2.tar.gz && tar -xvf v11.0.2.tar.gz
```

3. Create a folder for the build and enter it
```bash
mkdir geant4-11.0.2-build && cd geant4-11.0.2-build
```

4. Run the following commands to build Geant4.
```bash
cmake -DCMAKE_INSTALL_PREFIX=~/Geant4.11.0.2/geant4-11.0.2-install ~/Geant4.11.0.2/geant4-11.0.2  -DGEANT4_USE_OPENGL_X11=ON -DGEANT4_USE_QT=ON -DGEANT_BUILD_MULTITHREADED=ON -DGEANT4_INSTALL_DATA=ON
make -j8
make install
```

Some caveats: `-j8` means that 8 processes are used for the build, which may be too much for the PC if you're running other programs in the meantime. If the process slows down your PC by a lot, try `-j4`. Moreover, pay attention to **where** you are building Geant4. If you're **not** located in the WSL 2 standard home directory (and subfolders), i.e. you are in `/mnt/c/something` instead of `/home/username/something`, the building process may slow down and stop at around 29% (at the `Scanning dependencies of target G4processes` step) for many hours. This happens due to how WSL works and how the I/O is defined with the `/mnt` folders. Thus, it is highly recommended to build Geant in the `/home/username/` folder.

5. For setting up all the variables, a source should be performed every time you start up your machine. It is convenient to add the following lines in `~/.bashrc` file, a script which is executed each time you login (*the path should be modified depending on where you are installing everything; keep in mind that `~` is the home folder)
```bash
export G4INSTALL=~/Geant4-11.0.2/geant4-11.0.2-install
source $G4INSTALL/bin/geant4.sh
```

# How to run the simulation

Preliminary note: the simulation files which are run by the here-described `./exampleB1 run1.mac` command will **always** be located in the `CERN2022-build` folder. If you need to modify the core of the simulation (i.e., what processes are involved, how the setup is composed, which quantities must be saved on an event-by-event basis, etc.), the scripts that must be modified are the ones inside the `CERN2022` folder. Then, the `CERN2022-build` folder content must be recreated and re-run. The only times that the entire recreation process is **not** required are the cases in which you want to modify the **beam** used in the simulation (i.e., beam particle, beam energy, beam angular distribution, number of events to be acquired). 

When you have defined the `CERN2022` content, you must then do:

1. Go to the "CERN2022-build" folder and then:

```bash
source ~/Geant4.11.0.2/geant4-11.0.2-install/bin/geant4.sh
cmake -DGeant4_DIR=~/Geant4.11.0.2/geant4-11.0.2-install/lib/Geant4-11.0.2/ ../CERN2022 && make -j4
```

By default, the simulation will run in multi-thread mode. If this is a problem for any reason, it is possible to change the number of thread used through the command:
```bash
export G4FORCENUMBEROFTHREADS=4
```

2. **Normal execution**. Call the simulation as:
```bash
./exampleB1 run1.mac
```

3. **Run for graphics:**. Modify the `run1.mac` file setting the number of events to 1 and then:
```bash
./exampleB1
/control/execute run1.mac
```

**Note**: currently Geant4 can't render electron beams with divergences inferior to 3 mrad in both directions. If the divergence is set under this limit, XLaunch will simply freeze out.

# How to edit the simulation

More details in the `CERN2022` folder.